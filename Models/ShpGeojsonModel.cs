﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace GeoMongoServer.Models
{
    public class ShpGeojsonModel
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string id { get; set; }
        public string type { get; set; }
        public object geometry { get; set; }
        public object properties { get; set; }
        [BsonElement("id")]
        public int idFeature { get; set; }

    }
}
